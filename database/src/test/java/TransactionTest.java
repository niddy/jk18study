import cn.edu.zut.cs.Business.SaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.Test;

@ContextConfiguration("classpath*:database-context.xml")
public class TransactionTest extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    SaleService service;


    @Test
    @Rollback(false)
    public void transTest() {
        service.onSale(0.5f);
    }
}
