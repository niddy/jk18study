import cn.edu.zut.cs.Business.SaleService;
import cn.edu.zut.cs.Model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertTrue;

@ContextConfiguration("classpath*:database-context.xml")
public class BeanTest extends AbstractTestNGSpringContextTests {

    @Autowired
    Book book;

    @Autowired
    SaleService service;

    @Test
    public void beantest() {
        book.setName("java ee");
        assertTrue(book!=null);
    }

    @Test
    public void insert() {
        book.setIsdn("1234888");
        book.setName("transaction");
        book.setPrice(100);
        service.insert(book);
    }

    @Test
    public void onSale() {
        service.onSale(0.5f);
    }


}
