package cn.edu.zut.cs.DAO;

import cn.edu.zut.cs.Model.Book;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BookDAO {
    void insertBook(Book book);
    void modifyBook(Book book);
    List<Book> queryAll();
    void insertAll(List<Book> bookList);

    void deleteBook(Book book);
    void deleteAll(List<Book> bookList);
    void deleteById(String id);
    Book queryOneBook(Book book);
    Book queryOneBookById(String id);
}
