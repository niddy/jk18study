import cn.edu.zut.cs.Business.SaleService;
import cn.edu.zut.cs.Model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = "/HelloServlet")
public class HelloServlet extends HttpServlet {

    SaleService service;
    Book book;
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String isdn = request.getParameter("isdn");
        String name = request.getParameter("name");
        String author = request.getParameter("author");
        float price = Float.parseFloat(request.getParameter("price"));

        book.setIsdn(isdn);
        book.setName(name);
        book.setPrice(price);

        service.insert(book);

        PrintWriter out = response.getWriter();

        out.println(isdn);
        out.println(name);
        out.println(author);
        out.println(price);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();

        out.println("From Servlet!");

    }

    @Autowired
    public void setService(SaleService service) {
        this.service = service;
    }
    @Autowired
    public void setBook(Book book) {
        this.book = book;
    }
}
