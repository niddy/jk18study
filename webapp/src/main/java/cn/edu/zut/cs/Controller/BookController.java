package cn.edu.zut.cs.Controller;

import cn.edu.zut.cs.Business.SaleService;
import cn.edu.zut.cs.Model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class BookController {

    SaleService service;

    @RequestMapping(value = "/index.html")
    public String index() {
        return "index";
    }

    @RequestMapping(value = "/booklist.html")
    public String booklist(HttpServletRequest request) {

        request.getSession().setAttribute("books", service.queryAll() );
        return "booklist";
    }

    @RequestMapping(value = "/newbook.html")
    public String newbook() {
        return "newBook";
    }

    @RequestMapping(value = "/insert.html")
    public String insert(HttpServletRequest request, Book book) {
        service.insert(book);
        request.getSession().setAttribute("books", service.queryAll() );
        return "booklist";
    }

    @Autowired
    public void setService(SaleService service) {
        this.service = service;
    }
}
